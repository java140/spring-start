package ua.anton.springlearn;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ua.anton.springlearn")
@PropertySource("classpath:musicPlayer.properties")
public class SpringConfig {
    /* @Bean
    public ClassicalMusic classicalMusic() {
        return new ClassicalMusic();
    }


     @Bean
    public RockMusic classicalMusic() {
        return new ClassicalMusic();
        }
    */
}
